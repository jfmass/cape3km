from __future__ import division
import matplotlib as mpl
mpl.use('Agg')
import pygrib
import math, sys, os
import metpy.calc as mc
from subprocess import Popen, PIPE
import numpy as np
import numpy.ma as ma
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
sys.path.insert(0, '/home/meteo')
from cape import thermo, dynam
from matplotlib.colors import LinearSegmentedColormap
from scipy.ndimage.filters import gaussian_filter


class profObjIdx(object):
	
	def __init__(self,filepath,model,gtype,levels,debug):
		
		
		#get from file
		with Popen(['/home/meteo/uems/util/bin/wgrib2', filepath], stdout=PIPE, stderr=PIPE) as r:
			output, errore = r.communicate()
		prsrec = output.decode('utf-8').splitlines()
		grbit = pygrib.open(filepath)
		#get surface stuff
		self.grdHgt = np.asfortranarray(grbit[int([s for s in prsrec if 'HGT:surface' in s][0].split(':')[0])].values)
		self.lat, self.lon = grbit[int([s for s in prsrec if 'HGT:surface' in s][0].split(':')[0])].latlons()
		self.dx, self.dy = mc.lat_lon_grid_deltas(self.lon,self.lat)
		self.twoMtrDpt = np.asfortranarray(grbit[int([s for s in prsrec if 'DPT:2 m above ground' in s][0].split(':')[0])].values)
		self.twoMtrTmp = np.asfortranarray(grbit[int([s for s in prsrec if 'TMP:2 m above ground' in s][0].split(':')[0])].values)
		self.sfcQ = np.asfortranarray(grbit[int([s for s in prsrec if 'SPFH:2 m above ground' in s][0].split(':')[0])].values)
		self.grdPrs = np.asfortranarray(grbit[int([s for s in prsrec if 'PRES:surface' in s][0].split(':')[0])].values)
		self.mslp = np.asfortranarray(grbit[int([s for s in prsrec if 'MSLMA:mean sea level' in s][0].split(':')[0])].values)
		self.ugrd10m = np.asfortranarray(grbit[int([s for s in prsrec if 'UGRD:10 m above ground' in s][0].split(':')[0])].values)
		self.vgrd10m = np.asfortranarray(grbit[int([s for s in prsrec if 'VGRD:10 m above ground' in s][0].split(':')[0])].values)
		self.sfcvort = np.asarray(mc.vorticity(self.ugrd10m,self.vgrd10m,self.dx,self.dy))*10000
		print(np.amax(self.sfcvort))
		print(self.sfcvort.dtype)
		self.ushr6 = np.asfortranarray(grbit[int([s for s in prsrec if 'VUCSH:0-6000 m above ground' in s][0].split(':')[0])].values)
		self.vshr6 = np.asfortranarray(grbit[int([s for s in prsrec if 'VVCSH:0-6000 m above ground' in s][0].split(':')[0])].values)
		self.srh1 = grbit[int([s for s in prsrec if 'HLCY:1000-0 m above ground' in s][0].split(':')[0])].values
		self.srh3 = grbit[int([s for s in prsrec if 'HLCY:3000-0 m above ground' in s][0].split(':')[0])].values
		self.mlcape = np.asfortranarray(grbit[int([s for s in prsrec if 'CAPE:90-0 mb above ground' in s][0].split(':')[0])].values)  
		self.mlcinh = np.asfortranarray(grbit[int([s for s in prsrec if 'CIN:90-0 mb above ground' in s][0].split(':')[0])].values) 
		self.maxCape = np.asfortranarray(grbit[int([s for s in prsrec if 'CAPE:255-0 mb above ground' in s][0].split(':')[0])].values)      #useful to shorten calculations in cape3
		self.sbcape3 = np.asfortranarray(np.zeros(self.maxCape.shape))     #create base array for 3km cape, fills with 0s as base
		self.mlcape3 = np.asfortranarray(np.zeros(self.maxCape.shape))     #create base array for 3km cape, fills with 0s as base
		self.lsi = np.asfortranarray(np.full(self.maxCape.shape,99))     #create base array for lid strenght index, fills with 99s since 0 is a good value
		self.xmax,self.ymax = self.sbcape3.shape   
		self.tmpH = ma.zeros((50,self.xmax,self.ymax))
		self.spfhH = ma.zeros(self.tmpH.shape)
		self.hgtH = ma.zeros(self.tmpH.shape)
		self.prsH = ma.zeros(self.tmpH.shape)
		self.ugrdH = ma.zeros(self.tmpH.shape)
		self.vgrdH = ma.zeros(self.tmpH.shape)
		self.vert = {}
		self.grbit = grbit
		self.prsrec = prsrec
		self.retrieve = ['TMP','SPFH','HGT','PRES','UGRD','VGRD']
		if debug != 2:
			if gtype == 'native':
				n=0
				while n < levels:
					print("Level "+str(n))
					self.tmpH[n:]=np.asfortranarray(grbit[int([s for s in prsrec if 'TMP:'+str(n+1)+' hybrid level' in s][0].split(':')[0])].values)
					self.spfhH[n:]=np.asfortranarray(grbit[int([s for s in prsrec if 'SPFH:'+str(n+1)+' hybrid level' in s][0].split(':')[0])].values)
					self.hgtH[n:]=np.asfortranarray(grbit[int([s for s in prsrec if 'HGT:'+str(n+1)+' hybrid level' in s][0].split(':')[0])].values)
					self.prsH[n:]=np.asfortranarray(grbit[int([s for s in prsrec if 'PRES:'+str(n+1)+' hybrid level' in s][0].split(':')[0])].values)
					self.ugrdH[n:]=np.asfortranarray(grbit[int([s for s in prsrec if 'UGRD:'+str(n+1)+' hybrid level' in s][0].split(':')[0])].values)
					self.vgrdH[n:]=np.asfortranarray(grbit[int([s for s in prsrec if 'VGRD:'+str(n+1)+' hybrid level' in s][0].split(':')[0])].values)
					n = n+1
			elif gtype == 'pres':
				n=0
				for l in levels:
					self.tmpH[n:]=np.asfortranarray(grbit[int([s for s in prsrec if 'TMP:'+l+' mb' in s][0].split(':')[0])].values)
					svp=6.11*10**((7.5*(self.tmpH[n:]-273.15))/((self.tmpH[n:]-273.15)+237.3))
					rh=grbit[int([s for s in prsrec if 'RH:'+l+' mb' in s][0].split(':')[0])].values
					self.spfhH[n:]=np.asfortranarray(svp*rh/100)
					self.hgtH[n:]=np.asfortranarray(grbit[int([s for s in prsrec if 'HGT:'+l+' mb' in s][0].split(':')[0])].values)
					self.prsH[n:]=np.asfortranarray(float(l))
					self.ugrdH[n:]=np.asfortranarray(grbit[int([s for s in prsrec if 'UGRD:'+l+' mb' in s][0].split(':')[0])].values)
					self.vgrdH[n:]=np.asfortranarray(grbit[int([s for s in prsrec if 'VGRD:'+l+' mb' in s][0].split(':')[0])].values)
					n = n+1
			if debug == 1:
				print('Saving to npy files')
				np.ascontiguousarray(self.tmpH).dump('tmpH.npy')
				np.ascontiguousarray(self.spfhH).dump('spfH.npy')
				np.ascontiguousarray(self.hgtH).dump('ghtH.npy')
				np.ascontiguousarray(self.prsH).dump('prsH.npy')
				np.ascontiguousarray(self.ugrdH).dump('ugrdH.npy')
				np.ascontiguousarray(self.ugrdH).dump('vgrdH.npy')
		elif debug == 2:
			print('Loading from npy files')
			self.tmpH=np.asfortranarray(np.load('tmpH.npy'))
			self.spfH=np.asfortranarray(np.load('spfH.npy'))
			self.hgtH=np.asfortranarray(np.load('hgtH.npy'))
			self.prsH=np.asfortranarray(np.load('prsH.npy'))
			self.ugrdH=np.asfortranarray(np.load('ugrdH.npy'))
			self.vgrdH=np.asfortranarray(np.load('vgrdH.npy'))
		else:
			Print('Error in debug mode')
			
		
			


def chiT(mlcape,lsi,cape3,ncape,lr2,lr05,lcl,srh1,srh3,vort,meanfl):
	
	
	#Method 1
	#Cape3 part
	cp3=cape3
	cp3[cp3>150]=150    #Cap cape3 value
	cp3=cp3/100
	print(np.amax(cp3))
	
	#SHR1 part
	srhel1=srh1
	srhel1[srhel1>400]=400
	srhel1=srhel1/200
	print(np.amax(srhel1))
	
	#LCL part
	lclf=lcl
	lclf[lclf<1200]=1200
	lclf[lclf>2000]=2000
	lclf=(lclf-2000)/-800
	print(np.amax(lclf))
	
	#LSIpart
	lsif=lsi
	lsif[lsif>4.]=4.
	lsif[lsif<2.]=0.
	lsif=1-(lsif/4)
	print(np.amax(lsif))
	
	#2 km lapse rate part
	#lr2f=lr2
	#lr2f[lr2f<6]=7
	#lr2f=lr2f/7
	#print(np.amax(lr2f))
	
	#0.5km lapse rate part
	lr05f=lr05
	lr05f[lr05f>0]=1
	lr05f[lr05f<=0]=0
	print(np.amax(lr05f))
	
	met1=cp3*srhel1*lclf*lsif*lr05f
	print(np.amax(met1))
	
	#Method 2
	
	#SRH part
	#SHR1 part
	srhel1=srh1
	srhel1[srhel1<200]=0
	srhel1=srhel1/400
	
	
	#Cape part
	mcape1=mlcape
	mcape1[mcape1>1000]=1000
	mcape1=mcape1/600
	
	met2=srhel1*mcape1*lclf*lsif
	met1bst = met1
	met1bst[met1bst<1]=1
	met1bst[met1bst>1.5]=1.5
	met2=met2*met1bst
	print(np.amax(met2))
	
	
	#Method 3
	
	ncape1 = ncape/.3
	
	srhel1=srh1
	srhel1[srhel1>200]=200
	srhel1=srhel1/100 
	
	
	met3 = ncape1*srhel1*lsif*lclf
	
	
	#Method 4
	
	#srh3 part
	srh31 = srh3 / 300
	
	
	#cinh part
	lsif=lsi
	lsif[lsif>5.]=5.
	lsif[lsif<2.]=0.
	lsif=1-(lsif/5)
	
	met4 = ncape1*srh31*lr05f*lsif*cp3
	
	
	finchit = met1
	methld = np.ones(met1.shape, dtype=int)
	
	#Compare met2
	mettst=met2-met1
	methld = np.where(mettst>0,2,methld)
	finchit=np.fmax(finchit,met2)

	
	#compare met3
	mettst=met3-finchit
	methld = np.where(mettst>0,3,methld)
	finchit=np.fmax(finchit,met3)

	#compare met4
	mettst=met4-finchit
	methld = np.where(mettst>0,4,methld)
	finchit=np.fmax(finchit,met4)
	finchit = met1
	print(np.amax(finchit))
	
	#Enhancement
	
	vort1 = vort/12
	vort1[vort1<1]=1
	vort1[vort1>1.5]=1.5
	
	#finchit = finchit * vort1
	
	finchit[finchit>2]=2
	#finchit[finchit<0.3]=0
	print(np.amax(finchit))
	
	#Sig/vlnt decision threshld
	
	ncape1=np.where(methld==2,1,ncape1)
	ncape1[ncape1<1]=1
	
	
	#meanfl = meanfl/7.5
	#meanfl[meanfl<1]=1
	
	sr13 = (srh3-srh1)/75
	sr13[sr13<1]=1
	
	enh = ncape*sr13
	
	finchit=finchit*enh
	
	return finchit
	
	

def makeMap(var, typev, ubrb, vbrb, lats, lons, title, minl, maxl, cmap, tstamp):
		
		#minl is levels interval if type is cont

	if typev == 'filled':

		#hi res
		#plt.figure(figsize=(48, 32), dpi=256,frameon=False)
		fig = plt.figure(figsize=(48, 32), dpi=256,frameon=False)
		ax = fig.gca()
		ax.axis('off')
		ax.set_xlim([0, 1])
		ax.set_ylim([0, 1])
		plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
		m = Basemap(epsg=3857,llcrnrlon=(360-84.375),llcrnrlat=40.979898069620134,urcrnrlon=(360-67.5),urcrnrlat=48.922499263758255,resolution='i')
		x, y = m(lons, lats)
		cs = m.contourf(x,y,var,levels=ubrb,cmap=cmap,vmin=minl,vmax=maxl)
		#cs = m.contourf(x,y,var)
		#m.drawcoastlines()
		#m.drawstates()
		#m.drawrivers()
		#m.drawcountries()
		#m.colorbar()
		plt.savefig('/home/meteo/nowcast/imgs/nowcast_'+tstamp+'/'+title+'01hr.png', dpi=256, transparent=True)
		plt.close()	
		#del fig
		
		#med res
		plt.figure(figsize=(12, 8), dpi=256,frameon=False)
		fig = plt.figure(figsize=(12, 8), dpi=256,frameon=False)
		ax = fig.gca()
		ax.axis('off')
		ax.set_xlim([0, 1])
		ax.set_ylim([0, 1])
		plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
		m = Basemap(epsg=3857,llcrnrlon=(360-84.375),llcrnrlat=40.979898069620134,urcrnrlon=(360-67.5),urcrnrlat=48.922499263758255,resolution='i')
		x, y = m(lons, lats)
		cs = m.contourf(x,y,var,levels=ubrb,cmap=cmap,vmin=minl,vmax=maxl)
		plt.savefig('/home/meteo/nowcast/imgs/nowcast_'+tstamp+'/'+title+'01mr.png', dpi=256, transparent=True)
		plt.close()
	
	if typev == 'cont':
		
		
		nlvl = int(np.amax(var)-np.amin(var))
		print(nlvl)
		var = gaussian_filter(var,1)
		
		#hi res
		fig = plt.figure(figsize=(48, 32), dpi=256,frameon=False)
		ax = fig.gca()
		ax.axis('off')
		ax.set_xlim([0, 1])
		ax.set_ylim([0, 1])
		plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
		m = Basemap(epsg=3857,llcrnrlon=(360-84.375),llcrnrlat=40.979898069620134,urcrnrlon=(360-67.5),urcrnrlat=48.922499263758255,resolution='i')
		x, y =m(lons, lats)
		#udat, vdat, xv, yv = m.transform_vector(ubrb,vbrb,lons,lats,20,20,returnxy=True)

		cs = m.contour(x,y,var,levels=ubrb,lindewidths=0.2,cmap=cmap)
		plt.clabel(cs, inline=1, fontsize=8, fmt='%1.1f')
		plt.savefig('/home/meteo/nowcast/imgs/nowcast_'+tstamp+'/'+title+'01hr.png', dpi=256, transparent=True)
		plt.close()
		
		#med res
		fig = plt.figure(figsize=(12, 8), dpi=256,frameon=False)
		ax = fig.gca()
		ax.axis('off')
		ax.set_xlim([0, 1])
		ax.set_ylim([0, 1])
		plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
		m = Basemap(epsg=3857,llcrnrlon=(360-84.375),llcrnrlat=40.979898069620134,urcrnrlon=(360-67.5),urcrnrlat=48.922499263758255,resolution='i')
		x, y =m(lons, lats)
		#udat, vdat, xv, yv = m.transform_vector(ubrb,vbrb,lons,lats,20,20,returnxy=True)
		cs = m.contour(x,y,var,levels=ubrb,cmap=cmap)
		plt.clabel(cs, inline=1, fontsize=8, fmt='%1.1f')
		plt.savefig('/home/meteo/nowcast/imgs/nowcast_'+tstamp+'/'+title+'01mr.png', dpi=256, transparent=True)
		plt.close()
	
	
	if typev == 'barb':
		#hi res
		fig = plt.figure(figsize=(48, 32), dpi=256,frameon=False)
		ax = fig.gca()
		ax.axis('off')
		ax.set_xlim([0, 1])
		ax.set_ylim([0, 1])
		plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
		m = Basemap(epsg=3857,llcrnrlon=(360-84.375),llcrnrlat=40.979898069620134,urcrnrlon=(360-67.5),urcrnrlat=48.922499263758255,resolution='i')
		x, y =m(lons, lats)
		#udat, vdat, xv, yv = m.transform_vector(ubrb,vbrb,lons,lats,20,20,returnxy=True)
	
		#cs = m.barbs(x[::5,::5],y[::5,::5],ubrb[::5,::5],vbrb[::5,::5])
		cs = m.barbs(x,y,ubrb,vbrb,length=3.5,linewidth=0.2,sizes=dict(height=0.6))
		plt.savefig('/home/meteo/nowcast/imgs/nowcast_'+tstamp+'/'+title+'01hr.png', dpi=256, transparent=True)
		plt.close()
		
		#med res
		fig = plt.figure(figsize=(12, 8), dpi=256,frameon=False)
		ax = fig.gca()
		ax.axis('off')
		ax.set_xlim([0, 1])
		ax.set_ylim([0, 1])
		plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
		m = Basemap(epsg=3857,llcrnrlon=(360-84.375),llcrnrlat=40.979898069620134,urcrnrlon=(360-67.5),urcrnrlat=48.922499263758255,resolution='i')
		x, y =m(lons, lats)
		#udat, vdat, xv, yv = m.transform_vector(ubrb,vbrb,lons,lats,20,20,returnxy=True)
	
		cs = m.barbs(x[::4,::4],y[::4,::4],ubrb[::4,::4],vbrb[::4,::4],length=3.5,linewidth=0.2,sizes=dict(height=0.6))
		#cs = m.barbs(x,y,ubrb,vbrb,length=3.5,linewidth=0.2,sizes=dict(height=0.6))
		plt.savefig('/home/meteo/nowcast/imgs/nowcast_'+tstamp+'/'+title+'01mr.png', dpi=256, transparent=True)
		plt.close()
	
	
	#try:
		#cc = m.contour(x,y,contourvar, colors=('gray'))
	#except:
		#pass
	#br = m.barbs(xv,yv,udat,vdat)
	
	#m.drawcoastlines()
	#m.drawstates()
	#m.drawcountries()
	
	#br = m.barbs(x[::5,::5],y[::5,::5],ubrb[::5,::5],vbrb[::5,::5])
	
	#norm = mpl.colors.Normalize(vmin=minl, vmax=maxl)
	
	#plt.pcolor(x, y, var, cmap=cmap, vmin=minl, vmax=maxl)

	#cbar = plt.colorbar(ticks=[0,25,50,100,150,200])
	#cbar = m.colorbar(location='bottom',pad="5%",ticks=[0,25,50,100,150,200])
	#cbar.ax.tick_params(labelsize=50)
	#cbar.set_label('J/kg*m3',size=50)
	#plt.title(title,size=50)
	#plt.savefig('/home/meteo/capeimgs/'+title+'.png')
	#plt.close()	

def chiTMap(chiT, maxlat, minlat, minlon, maxlon, lats, lons, cmap, tstamp):
	levels = [0.3,1,2,3,4]
	styles = ['dashed','solid','solid','solid','solid']
	m=Basemap(llcrnrlon=minlon,llcrnrlat=minlat,urcrnrlon=maxlon,urcrnrlat=maxlat,resolution='h')
	x, y =m(lons, lats)
	cs = m.contour(x,y,chiT,levels,linestyles=styles)
	m.drawstates()
	m.drawrivers()
	m.drawcountries()
	m.drawcoastlines()
	plt.savefig('/home/meteo/nowcast/chit_'+tstamp+'.png')
	
	
	

def colrMp(prod):
	if (prod == 'blacks'):
		cdict = {'red':   ((0.0, 0.2, 0.2),
			(1.0, 0.0, 0.0)),
			
			'green': ((0.0, 0.2, 0.2),
			(1.0, 0.0, 0.0)),
			
			'blue':  ((0.0, 0.2, 0.2),
			(1.0, 0.2, 0.2)),
			
			'alpha': ((0.0, 0.2, 0.2),
			(1.0, 0.2, 0.2))
			}
	if (prod == '3cape'):
		cdict = {'red':   ((0.0, 1.0, 1.0),
			 (0.25, 0.26, 0.26),
			 (0.5, 1.0, 1.0),
			(1.0, 1.0, 1.0)),
			
			'green': ((0.0, 1.0, 1.0),
			(0.25, 0.63, 0.63),
			(0.5, 1.0, 1.0),
			(0.75, 0.26, 0.26),
			(1.0, 0.54, 0.54)),
			
			'blue':  ((0.0, 1.0, 1.0),
			(0.25, 0.63, 0.63),
			(0.5, 0.0, 0.0),
			(0.75, 0.0, 0.0),
			(1.0, 1.0, 1.0)),
			
			'alpha': ((0.0,0.0,0.0),
			(0.25,1.0,1.0),
			(1.0,1.0,1.0))
			}
	if (prod == '2mtmp'):
		cdict = {'red': ((0.0,1,1),
			(0.275,0.725,0.216),
			(0.5,0.431,0),
			(0.6875,0.96,0.96),
			(0.875,0.92,0.51),
			(1.0,1,1)),
			
			'green': ((0.0,1,1),
			(0.275,0.314,0.333),
			(0.5,0.803,0.745),
			(0.6875,0.922,0.922),
			(0.875,0.118,0),
			(1.0,1,1)),
			
			'blue': ((0.0,1,1),
			(0.275,0.627,0.647),
			(0.5,0.862,0.0),
			(0.6875,0.059,0.059),
			(0.875,0.137,0),
			(1.0,1,1))}
	cmap = LinearSegmentedColormap(prod,cdict)
	return cmap
	


	
path = sys.argv[1]
pathout = sys.argv[2]
tstamp = sys.argv[3]
gtype = sys.argv[4]
levels=sys.argv[5]
debug=int(sys.argv[6])

if isinstance(levels, str):
	levels = int(levels)


if debug != 4 :

	print('Creating profile base object')
	prof = profObjIdx(path,'hrrr',gtype,levels,debug)

	print('Done creating profile object')


	#cape3, lsi, lrarr, ncape6, lcl = cape3.cape3.severe(prof.maxCape,prof.tmpH,prof.spfhH,prof.prsH,prof.hgtH,prof.grdHgt,prof.twoMtrTmp)
	prof.mlcape3, prof.lsi, lrarr, ncape6, lcl = thermo.thermo.severe(prof.maxCape,prof.tmpH,prof.spfhH,prof.prsH,prof.hgtH,prof.grdHgt,prof.twoMtrTmp,prof.grdPrs,prof.sfcQ)

	if debug == 3:
		prof.mlcape3.dump('mlcape3.npy')
		lsi.dump('lsi.npy')
		lrarr.dump('lrarr.npy')
		ncape6.dump('ncape.npy')
		
	elif debug == 4:
		prof.mlcape3=np.load('mlcape3.npy')
		#lsi=
		
		


#makeMap(prof.sfcvort,'filled',prof.ugrd10m,prof.vgrd10m,prof.lat,prof.lon,'VORT',0,10,cmap,tstamp)
#makeMap(prof.dx,'filled',prof.ugrd10m,prof.vgrd10m,prof.lat,prof.lon,'DX',0,10,cmap,tstamp)
mw6u, mw6v, umot, vmot, duma, dumb, mnmag = dynam.dynam.kine(prof.hgtH,prof.ugrdH,prof.vgrdH,prof.ushr6,prof.vshr6,prof.grdHgt,prof.prsH)
print(mnmag)
print(np.amax(mnmag))


chit = chiT(prof.mlcape,prof.mlcinh,prof.mlcape3,ncape6,lrarr[2],lrarr[0],lcl,prof.srh1,prof.srh3,prof.sfcvort,mnmag)
print('Printing products')


cmap = colrMp('3cape')
levels = [0.3,1,2,3,4]
makeMap(chit,'cont',levels,prof.vgrd10m,prof.lat,prof.lon,'CHIT',0.,4.,cmap,tstamp)


#3cape
cmap = colrMp('3cape')
levels = 60
makeMap(prof.mlcape3,'filled',levels,prof.vgrd10m,prof.lat,prof.lon,'MLCAPE3',25.,200.,cmap,tstamp)


#Temp
cmap = colrMp('2mtmp')
levels = 41
makeMap(prof.twoMtrTmp-273.15,'filled',levels,prof.vgrd10m,prof.lat,prof.lon,'SFCTMP',-40.,40.,cmap,tstamp)

#10wbrb
makeMap(prof.twoMtrTmp-273.15,'barb',prof.ugrd10m,prof.vgrd10m,prof.lat,prof.lon,'10wbrb',0.,200.,cmap,tstamp)

#srh1
cmap = colrMp('blacks')
levels = [75, 100, 125, 150, 200, 250, 300]
makeMap(prof.twoMtrTmp-273.15,'cont',levels,prof.vgrd10m,prof.lat,prof.lon,'srh1',0.,500.,cmap,tstamp)



#print(np.amax(cape3))

#cmap = colrMp('3cape')


#makeMap(cape3,'filled',0,prof.ugrd10m,prof.lat,prof.lon,'MLCAPE3',25,200,cmap,tstamp)

#cmap = colrMp('2mtmp')

#prof.twoMtrTmp = prof.twoMtrTmp -273.15

#makeMap(prof.twoMtrTmp,'filled',0,prof.ugrd10m,prof.lat,prof.lon,'2MTMP',-40,40,cmap,tstamp)


#makeMap(prof.twoMtrTmp,'barb',prof.ugrd10m*1.944,prof.vgrd10m*1.944,prof.lat,prof.lon,'10wbrb',-40,40,cmap,tstamp)


#mslp = np.round(prof.mslp*0.01,1)
#print(mslp)

#makeMap(mslp,'cont',4,prof.vgrd10m*1.944,prof.lat,prof.lon,'mslp',800,1200,'black',tstamp)


#makeMap(lsi,prof.lat,prof.lon,'SBCAPE3',[0,1,2,3,5,7])


