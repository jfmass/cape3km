MODULE PRESHYB
    
    
    
    CONTAINS
    
    
    SUBROUTINE PRESTHYB(OLVLS,ILEN,JLEN,HARR,PARR,UARR,VARR,TARR,SPFHARR,HSFC,&
        &PSFC,USFC,VSFC,TSFC,SHSFC,FLVLS,FHARR,FPARR,FUARR,FVARR,FTARR,&
        &FSPFHARR)
    
    
    USE INTERP
    
    IMPLICIT NONE
    
    
    
    INTEGER, INTENT(IN) :: OLVLS, ILEN, JLEN, FLVLS
    REAL, DIMENSION(OLVLS,ILEN,JLEN), INTENT(IN) :: HARR,PARR,UARR,VARR,TARR,SPFHARR
    REAL, DIMENSION(FLVLS,ILEN,JLEN), INTENT(OUT) :: FHARR,FPARR,FUARR,FVARR,FTARR,FSPFHARR
    REAL, DIMENSION(ILEN,JLEN), INTENT(IN) :: HSFC,PSFC,USFC,VSFC,TSFC,SHSFC
    REAL, DIMENSION(FLVLS) :: ELVLS
    REAL, ALLOCATABLE :: RPARR(:), ROARR(:)
    REAL, DIMENSION(FLVLS) :: RFPARR, RTMP
    INTEGER :: K, I, J, FRST, RLVLS
    REAL :: PTOP 
    
    
    !First define pressure at every level
    
    ELVLS = (/0.9980, 0.9940, 0.9870, 0.9750, 0.9590,&
        &0.9390, 0.9160, 0.8920, 0.8650, 0.8350, 0.8020, 0.7660,&
          &0.7270, 0.6850, 0.6400, 0.5920, 0.5420, 0.4970, 0.4565,&
          &0.4205, 0.3877, 0.3582, 0.3317, 0.3078, 0.2863, 0.2670,&
          &0.2496, 0.2329, 0.2188, 0.2047, 0.1906, 0.1765, 0.1624,&
          &0.1483, 0.1342, 0.1201, 0.1060, 0.0919, 0.0778, 0.0657,&
          &0.0568, 0.0486, 0.0409, 0.0337, 0.0271, 0.0209, 0.0151,&
          &0.0097, 0.0047, 0.0000 /)
    PTOP = 2000.

!omp parrallel do private(j,i,k,rlvls,frst,rparr,roarr,rtmp)
    DO J=1, JLEN
        DO I=1, ILEN
            
            
            DO K=1, FLVLS
                
                !define pressure at levels
                
                IF (K == 1) THEN
                    FPARR(K,I,J) = (ELVLS(K)*(PSFC(I,J)-PTOP)+PTOP + PSFC(I,J)) / 2
                ELSE
                    FPARR(K,I,J) = (ELVLS(K)*(PSFC(I,J)-PTOP)+PTOP + FPARR(K-1,I,J)) / 2
                END IF

            END DO       
            
            RFPARR = FPARR(FLVLS:1:-1,I,J)
            
            
            !find how many real levels we do have
            DO K=1, OLVLS
                IF (PARR(K,I,J) > PSFC(I,J)) THEN
                    CYCLE
                END IF
                
                FRST = K
                RLVLS = OLVLS-K
                EXIT            
            END DO
            
            
            ALLOCATE(RPARR(RLVLS),ROARR(RLVLS))
            
            RPARR = PARR(OLVLS:FRST:-1,I,J)
            
            !HEIGHT
            ROARR = HARR(OLVLS:FRST:-1,I,J)       
            CALL STEFFEN(RLVLS,RPARR,ROARR,FLVLS,RFPARR,RTMP)
            FHARR(:,I,J) = RTMP(FLVLS:1:-1)
            
            !TMP
            ROARR = TARR(OLVLS:FRST:-1,I,J)
            CALL STEFFEN(RLVLS,RPARR,ROARR,FLVLS,RFPARR,RTMP)
            FTARR(:,I,J) = RTMP(FLVLS:1:-1)
            
            !SPFH
            ROARR = SPFHARR(OLVLS:FRST:-1,I,J)
            CALL STEFFEN(RLVLS,RPARR,ROARR,FLVLS,RFPARR,RTMP)
            FSPFHARR(:,I,J) = RTMP(FLVLS:1:-1)
        
            !U
            ROARR = UARR(OLVLS:FRST:-1,I,J)
            CALL STEFFEN(RLVLS,RPARR,ROARR,FLVLS,RFPARR,RTMP)
            FUARR(:,I,J) = RTMP(FLVLS:1:-1)
            
            !V
            ROARR = VARR(OLVLS:FRST:-1,I,J)
            CALL STEFFEN(RLVLS,RPARR,ROARR,FLVLS,RFPARR,RTMP)
            FVARR(:,I,J) = RTMP(FLVLS:1:-1)
        
        
        END DO
    END DO
    
    
    
    
    
    
    
    
    END SUBROUTINE


END MODULE