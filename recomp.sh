#!/bin/bash
cd severe/cape3km
git pull --no-edit origin master
cp cape3.F90 ../../cape/cape3.F90
cp interp.F90 ../../cape/interp.F90
cp dynam.F90 ../../cape/dynam.F90
cp prestohybrid.F90 ../../cape/prestohybrid.F90
cp thermo.f90 ../../cape/thermo.f90
cp thermutils.f90 ../../cape/thermutils.f90

cd ../../cape
#f2py3 -m cape3 --f90flags='-O3 -fopenmp' --debug -lgomp -c cape3.F90 &> cape3.log

gfortran -c interp.F90 &> interp.log 
gfortran -c thermutils.f90 &> thermutils.log
f2py3 -m dynam --f90flags='-O3 -fopenmp' --debug -lgomp -c interp.F90 dynam.F90 &> dynam.log

f2py3 -m prestohybrid --f90flags='-O3 -fopenmp' --debug -lgomp -c interp.F90 prestohybrid.F90 &> prestohybrid.log

f2py3 -m thermo --f90flags='-O3 -fopenmp' --debug -lgomp -c interp.F90 thermutils.f90 thermo.f90 &> thermo.log


#mv cape3.cpython-35m-x86_64-linux-gnu.so cape3.so
mv dynam.cpython-35m-x86_64-linux-gnu.so dynam.so
mv prestohybrid.cpython-35m-x86_64-linux-gnu.so prestohybrid.so
mv thermo.cpython-35m-x86_64-linux-gnu.so thermo.so
cd ..