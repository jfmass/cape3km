MODULE THERMOD

      CONTAINS
      
      SUBROUTINE SEVERE(ILEN,JLEN,LVLS,MLCAPE, &
                        &TARR,QARR,PARR,HARR,  &
                        &SFCARR,SFCTMP,SFCPRS,SFCQ,CAPE3,LSI, &
                        &LRARR,NC6KM,HLCL,DEB,DI,DJ)


      !----------------------------------------------------------------------------
      !Routine built to calculate some severe weather parameters and wrap to python
      !By JF Massicotte
      !2017-11-05
      !
      !
      !
      !  Version 0.2
      !  2019-01-12
      !  Added 500m LAPSE RATE, 2KM LAPSE RATE, 6KM NORMALIZED CAPE
      !
      !  Version 0.1
      !  2018-09-09
      !  CAPE 3KM, LID STRENGTH INDEX
      !----------------------------------------------------------------------------
      
      USE THERMUTILS
      USE INTERP
      
      
      implicit none

      !Declare variables
      integer, intent(in) :: ILEN,JLEN,LVLS
      !integer, dimension(LVLS,ILEN,JLEN),intent(in) :: 
      real,    dimension(LVLS,ILEN,JLEN),intent(in) :: TARR,QARR,PARR,HARR  !arrays from args
      real,    dimension(ILEN,JLEN),intent(in) :: MLCAPE,SFCARR,SFCTMP,SFCPRS,SFCQ
      real*8,    dimension(ILEN,JLEN),intent(out) :: CAPE3, LSI,NC6KM, HLCL  !output arrays
      real*8,    dimension(4,ILEN,JLEN),intent(out)  ::  LRARR
      real,    dimension(ILEN,JLEN) :: MXLTMP    !LCL Prs,LCL tmp, LCL hgt, mltmp
      real,    allocatable :: TPAR(:),TENV(:),HENV(:),PENV(:),VTENV(:),VTPAR(:),WENV(:),DT(:),&
              &DIF(:),CAPEARR(:)
      real,    dimension(LVLS+1) :: TPROF, PPROF, QPROF, HPROF, WPROF
      real,    dimension(51) :: MXPRSS, MXQS, MXTMPS
      real,    dimension(4) :: LRH, LRT
      real,    dimension(1) :: PLCL, LCLINT
      real  ::    MXQ, MXTMP, MXTD, TLCL, WBPOT
      real,    parameter  ::  A=0.0038,B=17.2693882,C=35.86,TFR=273.15,G=9.80665
      integer    :: MXLVLS,LCL00,LCL10,LCL01,LCL11, &
                     &I,J,L,CP3FL,PARL,PARLL,PARLT, DEBUG
      integer, optional :: DEB, DI, DJ
      IF (PRESENT(DEB)) THEN
         DEBUG=DEB
      ELSE
         DEBUG=0
      END IF
                     
      
      
            !Init calculated arrays
      write(6,*) 'initiating arrays!'
!$omp  parallel do private (I,J,L)
      DO I=1,ILEN
        DO J=1,JLEN
          CAPE3(I,J)   = 0
          LSI(I,J)     = 5
          HLCL(I,J)    = 0
	      MXLTMP(I,J)  = 0
		  NC6KM(I,J)   = 0
          DO L=1, 4
            LRARR(L,I,J) = 0
          END DO
        END DO
      END DO
      
      
      
 !$omp  parallel do private (i,j,l,tprof,qprof,pprof,hprof,wprof,mxprss,mxqs,mxtmps,&
 !$omp &mxq,mxtmp,lrh,lrt,mxtd,tlcl,lclint,parl,parll,penv,tpar,tenv,henv,vtenv,vtpar,&
 !$omp &wenv,parlt,wbpot,dt,dif,capearr,plcl)     
      DO J=1, JLEN
        DO I=1, ILEN
          !PRINT *, I, J
          !build profile vars
          DO L=1, LVLS+1
            IF (L==1) THEN
              TPROF(L) = SFCTMP(I,J)
              QPROF(L) = SFCQ(I,J)
              PPROF(L) = SFCPRS(I,J)*0.01   !Pressure back to mb
              HPROF(L) = SFCARR(I,J)
            ELSE
              TPROF(L) = TARR(L-1,I,J)
              QPROF(L) = QARR(L-1,I,J)
              PPROF(L) = PARR(L-1,I,J)*0.01   !Pressure back to mb
              HPROF(L) = HARR(L-1,I,J)
            END IF
            WPROF(L) = QPROF(L)/(1-QPROF(L))
          END DO
          
            
                  
          
          
          
          !Define lapse rates
          !---------------------------------------------------------------
          LRH = (/ HPROF(1)+500,HPROF(1)+1000,HPROF(1)+2000,HPROF(1)+3000 /)
          CALL STEFFEN(LVLS+1,HPROF,TPROF,4,LRH,LRT,1)
          LRARR(:,I,J) = (LRT-TPROF(1))/(LRH-HPROF(1)) * (-1000.)
          
          
          !define mixing layer
          !---------------------------------------------------------------
          
          DO L=1, 51
            MXPRSS(L) = PPROF(1)-(REAL(L-1)*2.)
          END DO
          
          CALL STEFFEN(LVLS+1,PPROF(LVLS+1:1:-1),TPROF(LVLS+1:1:-1),51,MXPRSS,MXTMPS,2)
          CALL STEFFEN(LVLS+1,PPROF(LVLS+1:1:-1),WPROF(LVLS+1:1:-1),51,MXPRSS,MXQS,3)
          MXQ = SUM(MXQS)/51.
          MXTMP = SUM(MXTMPS*((PPROF(1)/MXPRSS)**0.286))/51.
          
          
          
          !Define LCL from T and mixing ratio
          !---------------------------------------------------------------
          CALL GETLCL(MXTMP,MXQ,PPROF,HPROF,LVLS+1,HLCL(I,J),PLCL(1))
          
          
          WBPOT = OW(TLCL-TFR,TLCL-TFR,PLCL(1))
          
          !Define how many levels in profile and build interpolated profile
          !----------------------------------------------------------------
          
          PARL = FLOOR((PPROF(1)-PLCL(1))/2.)
          
          IF (MOD(PPROF(1)-PLCL(1),2.) == 0.) THEN
            PARL = PARL+1
          ELSE
            PARL = PARL+2
          END IF
          
          PARLL = FLOOR((PLCL(1)-PPROF(LVLS+1))/2.)
          PARLT = PARL+PARLL
          
          ALLOCATE(PENV(PARLT),TPAR(PARLT),TENV(PARLT),HENV(PARLT),VTENV(PARLT),&
          &VTPAR(PARLT),WENV(PARLT),DT(PARLT),DIF(PARLT),CAPEARR(PARLT))
          
          
          !define pressure array to the top
          !---------------------------------------------------------------
          DO L=1, PARLT
            IF (L<PARL) THEN
              PENV(L) = PPROF(1)-(2.*(REAL(L-1)))
            ELSE
              PENV(L) = PLCL(1)-(2.*(REAL(L-PARL)))
            END IF
          END DO
          
          IF (PENV(PARLT)<PPROF(LVLS+1)) THEN
            PENV(PARLT) = PPROF(LVLS+1)
          END IF
          
          !define env tmp, hgt and w arrays to the top
          !---------------------------------------------------------------
          CALL STEFFEN(LVLS+1,PPROF(LVLS+1:1:-1),TPROF(LVLS+1:1:-1),PARLT,PENV,TENV,5)
          CALL STEFFEN(LVLS+1,PPROF(LVLS+1:1:-1),HPROF(LVLS+1:1:-1),PARLT,PENV,HENV,6)
          CALL STEFFEN(LVLS+1,PPROF(LVLS+1:1:-1),WPROF(LVLS+1:1:-1),PARLT,PENV,WENV,PARLL)
          HENV = HENV-HPROF(1)
          
          !parcel temp below LCL, at LCL and above LCL
          !---------------------------------------------------------------
          DO L=1, PARL-1
            IF (L==1) THEN
              TPAR(1)=MXTMP
            ELSE
              TPAR(L)=((PENV(L)-PENV(1))/(PENV(PARL)-PENV(1)))*(TLCL-MXTMP)+MXTMP
              VTPAR(L) = VTNOW(TPAR(L),MXQ)
              VTENV(L) = VTNOW(TENV(L),WENV(L))
            END IF
          END DO
          
          !parcel temp at LCL
          TPAR(PARL)=TLCL
          VTPAR(PARL)=VTNOW(TLCL,MXQ)
          VTENV(PARL) = VTNOW(TENV(PARL),WENV(PARL))
          
          !parcel temp above LCL
          DO L=PARL+1, PARLT
            TPAR(L)=SATLFT(WBPOT,PENV(L))+TFR
            VTPAR(L)= VIRTMP(PENV(L),TPAR(L))
            VTENV(L) = VTNOW(TENV(L),WENV(L))
          END DO
          
          
          
          !Compare parcel to environment
          !---------------------------------------------------------------------
          
          DT = TPAR-TENV
          DIF = DT/TENV
          CAPEARR = G * ((DIF+EOSHIFT(DIF,SHIFT=-1,BOUNDARY=0.))/2.)*(HENV-EOSHIFT(HENV,SHIFT=-1,BOUNDARY=0.))
          CAPE3(I,J) = SUM(PACK(PACK(CAPEARR,CAPEARR>0),PACK(HENV,CAPEARR>0)<3000.))
          LSI(I,J) = MINVAL(PACK(DT,HENV<3000.))*-1.
          NC6KM(I,J) = SUM(PACK(PACK(CAPEARR,CAPEARR>0),PACK(HENV,CAPEARR>0)<6000.))/6000.
          IF (DEBUG == 1) THEN
            IF (I == DI) THEN
              IF (J == DJ) THEN
                !PRINT *, MLCAPE(I,J), CAPEARR(1:50),CAPE3(I,J),MXTMP,HLCL(I,J),TLCL,&
                 ! &SUM(PACK(CAPEARR,CAPEARR>0)),MAXVAL(DT),MAXVAL(DIF),DT(1:50),NC6KM(I,J)
                 PRINT *, "PRS, HGT, TMP, TPAR, DT, DIF, CAPE"
                 DO L=1, PARLT
                   PRINT *, PENV(L), HENV(L), TENV(L), TPAR(L), DT(L), DIF(L), CAPEARR(L), LSI(I,J)
                 END DO
              END IF
            END IF
          END IF
          DEALLOCATE(PENV,TPAR,TENV,HENV,VTENV,VTPAR,WENV,DT,DIF,CAPEARR)
        
        END DO
      END DO

      END SUBROUTINE

      subroutine getlcl(tmp,mxq,pprof,hprof, lvls,hlcl,plcl)
        
        
        use thermutils
        use interp
        !Define LCL from T and mixing ratio
          !---------------------------------------------------------------
          real,    dimension(lvls),intent(in) :: pprof, hprof
          real,    intent(in)  ::  mxq, tmp
          real,    dimension(1) ::  plcl, lclint
          real,    intent(out)  ::  hlcl
          real  ::  td, tlcl
          real,    parameter  ::  A=0.0038,B=17.2693882,C=35.86,TFR=273.15
          
          
          td = (tfr-(C/B)*LOG(mxq/A))/(1-LOG(mxq/A)/B)
          if (td > tmp) then
            td = tmp
          end if
          tlcl = (1/(1/(td - 56.) + LOG(tmp/td)/800.)) + 56.
          plcl(1) = 1000/((tmp*((1000/pprof(1))**0.286))/tlcl)**(1/0.286)
          if (plcl(1) > pprof(1)) then
            plcl(1) = pprof(1)
          end if
          
          !Interpolate LCL height 
          CALL STEFFEN(lvls,pprof(lvls:1:-1),hprof(lvls:1:-1),1,plcl,lclint,FLOOR(TD))
          hlcl = lclint(1)-hprof(1)
        
        
      end subroutine

END MODULE
