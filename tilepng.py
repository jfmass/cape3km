#!/usr/bin/env python

from PIL import Image
import os
import glob
import sys

def tilethem():
	flpath = glob.glob("*.png")
	for png in flpath:
		print(os.getcwd())
		base = Image.open(png)
		fold = png[:-8]
		res = png[-6:]
		ff = png[-8:-6]
		try:
			os.mkdir(ff)
			os.chdir(ff)
		except:
			os.chdir(ff)
		try:
			os.mkdir(fold)
			os.chdir(fold)
		except:
			os.chdir(fold)
		if res == 'lr.png':
			x=2
			maxx=6
			y=5
			miny=5
			maxy=8
			z=4
			if not os.path.exists(str(z)):
				os.mkdir(str(z))
			os.chdir(str(z))
			i = 0
			while x < maxx:
				if not os.path.exists(str(x)):
					os.mkdir(str(x))
				os.chdir(str(x))
				j = 0
				while y < maxy:
					buffer = Image.new("RGBA", [256, 256], (0, 0, 0))
					tile = base.crop((i,j,i+256,j+256))
					buffer.paste(tile, (0,0))
					buffer.convert('P', palette=Image.ADAPTIVE, colors=255)
					buffer.save(str(y)+'.png', "PNG")
					j += 256
					y += 1
				i += 256
				x += 1
				y = miny
				os.chdir("..")
			#print str(i)
			#print str(j)
			os.chdir("..")
		elif res == 'mr.png':
			x=68
			maxx=80
			y=88
			miny=88
			maxy=96
			z=8
			if not os.path.exists(str(z)):
				os.mkdir(str(z))
			os.chdir(str(z))
			i = 0
			while x < maxx:
				if not os.path.exists(str(x)):
					os.mkdir(str(x))
				os.chdir(str(x))
				j = 0
				while y < maxy:
					buffer = Image.new("RGBA", [256, 256], (0, 0, 0))
					tile = base.crop((i,j,i+256,j+256))
					buffer.paste(tile, (0,0))
					buffer.convert('P', palette=Image.ADAPTIVE, colors=255)
					buffer.save(str(y)+'.png', "PNG")
					j += 256
					y += 1
				i += 256
				x += 1
				y = miny
				os.chdir("..")
			#print str(i)
			#print str(j)
			os.chdir("..")
		elif res == 'hr.png':
			x=272
			maxx=320
			y=352
			miny=352
			maxy=384
			z=10
			if not os.path.exists(str(z)):
				os.mkdir(str(z))
			os.chdir(str(z))
			i = 0
			while x < maxx:
				if not os.path.exists(str(x)):
					os.mkdir(str(x))
				os.chdir(str(x))
				j = 0
				while y < maxy:
					buffer = Image.new("RGBA", [256, 256], (0, 0, 0))
					tile = base.crop((i,j,i+256,j+256))
					buffer.paste(tile, (0,0))
					buffer.convert('P', palette=Image.ADAPTIVE, colors=255)
					buffer.save(str(y)+'.png', "PNG")
					j += 256
					y += 1
				i += 256
				x += 1
				y = miny
				os.chdir("..")
			#print str(i)
			#print str(j)
			os.chdir("..")
		else:
			print('Error with png file')
		os.chdir("..")
		os.chdir("..")
		os.remove(png)


		



#Get folder from args
folder = str(sys.argv[1])
#os.chdir('/home/meteo/nowcast/imgs/'+folder)

tilethem()



quit()

